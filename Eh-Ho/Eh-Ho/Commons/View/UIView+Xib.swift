//
//  UIView+Xib.swift
//  Eh-Ho

import UIKit

class UIXibView: UIView {
    
    var nibName:String? { return String(describing: type(of: self)) }
    
    var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setXIB()
        awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setXIB()
    }
    
    private func setXIB() {
        guard let nibName = self.nibName,
            let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView
            else { return }
        self.layoutIfNeeded()
        contentView = view
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        self.addSubview(contentView)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
