//
//  APIClient.swift

import Foundation

final class SessionAPI {
    
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        return session
    }()
    
    func send<T: APIRequest>(request: T, completion: @escaping(Result<T.Response, APIError>) -> Void) {
        let request = request.requestWithBaseURL()
        let task = self.session.dataTask(with: request) { (data, response, error) in
            do {
                if let data = data {
                    let model = try JSONDecoder().decode(T.Response.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(model))
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(.failure(.unknown))
                    }
                }
            } catch let error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    completion(.failure(.unknown))
                }
            }
        }
        task.resume()
    }
}
