//
//  APIError.swift

import Foundation

enum APIError: Error {
    case unknown
}
