//
//  APIRequest.swift

import Foundation

let APIURL = ""

enum Method: String {
    
    case GET
    case POST
    case PUT
    case UPDATE
    case DELETE
    
}

enum URLBASE {
    case rides
    case currency
}

protocol APIRequest: Encodable {
    associatedtype Response: Codable
    var method: Method { get }
    var baseURLRides: URL { get }
    var baseURLCurrency: URL { get }
    var path: String { get }
    var parameters: [String: String] { get }
    var bodyObjects: [String: Any] { get }
    var headers: [String: String] { get }
}

//Default implementation of APIRequest protocol
extension APIRequest {
    
    var baseURL: URL {
        guard let baseURL = URL(string: APIURL) else {
            fatalError("Impossible get baseURL for API")
        }
        return baseURL
    }
    
    func requestWithBaseURL() -> URLRequest {
        
        let URL = baseURL.appendingPathComponent(path)
        
        guard var components = URLComponents(url: URL, resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URLCompounent form \(URL)")
        }
        
        if !parameters.isEmpty {
            components.queryItems = parameters.map {
                URLQueryItem(name: $0, value: $1 )
            }
        }
        
        guard let finalURL = components.url else {
            fatalError("Unable to retrieve final URL")
        }
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        if !bodyObjects.isEmpty {
            let jsonData = try? JSONSerialization.data(withJSONObject: bodyObjects)
            request.httpBody = jsonData
        }
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        if let headerLanguage = headers["Accept-Language"] {
            request.addValue(headerLanguage, forHTTPHeaderField: "Accept-Language")
        }
        
        return request
    }
    
}
