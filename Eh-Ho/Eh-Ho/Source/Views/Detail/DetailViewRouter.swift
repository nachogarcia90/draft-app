//
//  DetailViewRouter.swift
//  Eh-Ho

import UIKit

class DetailViewRouter {
    
    weak var viewController: DetailViewViewController?
    
    static func getViewController() -> DetailViewViewController {
        
        let configuration = configureModule()
        
        return configuration.vc
        
    }
    
}

//MARK: - MVVM
extension DetailViewRouter {
    
    private static func configureModule() -> (vc: DetailViewViewController, vm: DetailViewViewModel, rt: DetailViewRouter) {
        
        let viewController = DetailViewViewController()
        let router = DetailViewRouter()
        let viewModel = DetailViewViewModel()
        
        viewController.viewModel = viewModel
        
        viewModel.router = router
        viewModel.view = viewController
        
        router.viewController = viewController
        
        return (viewController, viewModel, router)
        
    }
    
}
