//
//  DetailViewViewController.swift
//  Eh-Ho

import UIKit

class DetailViewViewController: UIViewController {
    
    var viewModel: DetailViewViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Your code here
    }
    
}

//MARK: - ViewModel communication
protocol DetailViewViewControllerProtocol: class {
    
}

extension DetailViewViewController: DetailViewViewControllerProtocol {
    
}
