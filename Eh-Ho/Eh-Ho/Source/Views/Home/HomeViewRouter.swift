//
//  HomeViewRouter.swift
//  Eh-Ho

import UIKit

class HomeViewRouter {
    
    weak var viewController: HomeViewViewController?
    
    static func getViewController() -> HomeViewViewController {
        
        let configuration = configureModule()
        
        return configuration.vc
        
    }
    
    func navigateToDetailScreen() {
        let vc = DetailViewRouter.getViewController()
        viewController?.present(vc, animated: true, completion: nil)
    }
    
}

//MARK: - MVVM
extension HomeViewRouter {
    
    private static func configureModule() -> (vc: HomeViewViewController, vm: HomeViewViewModel, rt: HomeViewRouter) {
        
        let viewController = HomeViewViewController()
        let router = HomeViewRouter()
        let viewModel = HomeViewViewModel()
        
        viewController.viewModel = viewModel
        
        viewModel.router = router
        viewModel.view = viewController
        
        router.viewController = viewController
        
        return (viewController, viewModel, router)
        
    }
    
}
