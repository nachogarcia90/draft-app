//
//  HomeViewViewController.swift
//  Eh-Ho

import UIKit

class HomeViewViewController: UIViewController {
    
    var viewModel: HomeViewViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        viewModel.didTapInButton()
    }
}

//MARK: - ViewModel communication
protocol HomeViewViewControllerProtocol: class {
    
}

extension HomeViewViewController: HomeViewViewControllerProtocol {
    
}
