//
//  HomeViewViewModel.swift
//  Eh-Ho

import Foundation

class HomeViewViewModel {
    
    weak var view: HomeViewViewControllerProtocol?
    var router: HomeViewRouter?
    
    func didTapInButton() {
        router?.navigateToDetailScreen()
    }
}
